from django.test import TestCase, Client
from django.test.client import RequestFactory
from django.urls import resolve
from .views import index, paginate_page
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper


class Lab7UnitTest(TestCase):
    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_friend_list_url_is_exist(self):
        response = Client().get('/lab-7/friend-list/')
        self.assertEqual(response.status_code, 200)

    def test_add_friend(self):
        name = "Mona Natasha"
        npm = "1606874601"
        response_s = Client().post('/lab-7/add-friend/',data={'name':name,'npm':npm})
        response_f = Client().post('/lab-7/add-friend/',data={'name':name,'npm':npm})
        self.assertEqual(response_s.status_code, 200)
        self.assertEqual(response_f.status_code,404)

    def test_delete_friend(self):
        Friend.objects.create(friend_name="Bebek",npm="1606892312")
        self.assertEqual(Friend.objects.all().count(),1)
        Client().get('/lab-7/delete-friend/1/')
        self.assertEqual(Friend.objects.all().count(),0)

    def test_validate_npm(self):
        npm = "1606892312"
        Friend.objects.create(friend_name="Bebek",npm=npm)
        response = Client().post('/lab-7/validate-npm/',data={'npm':npm})
        self.assertEqual(response.json()['is_taken'],True)

    def test_get_friend_list_url_is_exist(self):
        response = Client().get("/lab-7/get-friend-list/")
        self.assertEqual(response.status_code,200)

    def test_paginator_is_good(self):
        paginate_page("String",[1,2,31,2,1])
        paginate_page(2,[1,2,3,4])

    def test_helper_with_wrong_username(self):
        csui_helper = CSUIhelper()
        csui_helper.instance.username = "bebek"
        csui_helper.instance.password = "password"
        try:
            csui_helper.instance.get_access_token()
        except Exception as e:
            self.assertIn('username atau password sso salah',str(e))
